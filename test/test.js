const times10 = (n) => {
	return n * 10;
};

console.log('~~~TASK 0NE~~~');
console.log(times10(9));


const memoizedClosureTimes10 = () => {

	const cache = {};

	return (n) => {
		if (n in cache) {
			console.log('Fetching Result from Cache', n);
			return cache[n];
		} else {
			console.log('Calculating...!');
			return cache[n] = times10(n);
		}
	};
};

var cache = {
	// UserInput: Result
	100: 10,
};

// To get the user query



const memoClosureTimes10 = memoizedClosureTimes10();
console.log('~~~~~~~~~~~TASK THREE~~~~~~~~~~~');
try {
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
} catch (error) {
	console.log('Task 3:' + error);
}