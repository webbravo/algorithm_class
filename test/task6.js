// 	Write a recursive factorial function
var counter = 1;

function factorial(num) {
	// Work
	counter *= num;

	if (num <= 1) {
		return counter;
	}

	return num * factorial(num - 1);
}

console.log(factorial(9));


// const friendship = () => {

// 	const name = 'Feyi';
// 	const character = ['Smart', 'Caring', 'Funny', 'Beautiful'];

// 	function myFriendIs(name, character, index) {

// 		// 2. Identify recursive case
// 		console.log(`${name} is ${character[index]}`);

// 		// 1. When to stop (base case)
// 		if (index < character.length - 1) {
// 			// 3. Return when appriorate
// 			return myFriendIs(name, character, index + 1 /* 4. Bring me to base case */ );
// 		}
// 	}

// 	myFriendIs(name, character, 0);
// };

// friendship();