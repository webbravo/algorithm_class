function joinElements(array, joinString) {

	function recurse(index, resultSoFar) {
		resultSoFar += array[index];

		if (index === array.length - 1) {
			return resultSoFar;
		} else {
			return recurse(index + 1, resultSoFar + joinString);
		}
	}

	return recurse(0, '');
}

const arr = ['s', 'cr', 't cod', ' :) :)'];

console.log(joinElements(arr, 'e'));

console.log('==== LOOP THRU ITERATION ===');

function joinElementTwo(array, joinString) {
	let resultSoFar = '';
	for (let i = 0; i < array.length - 1; i++) {
		resultSoFar += array[i] + joinString;
	}
	return resultSoFar + array[array.length - 1];
}

console.log(joinElementTwo(arr, 'e'));