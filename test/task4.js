const times10 = (n) => {
	return n * 10;
};

console.log('~~~TASK 0NE~~~');
console.log(times10(9));


const memoizedClosureTimes10 = (cb) => {

	const cache = {};

	return (n) => {
		if (n in cache) {
			console.log('Fetching Result from Cache', n);
			return cache[n];
		} else {
			console.log('Calculating...!');
			return cache[n] = cb(n);
		}
	};
};


const memoClosureTimes10 = memoizedClosureTimes10(times10);
console.log('~~~~~~~~~~~TASK FOUR~~~~~~~~~~~');
try {
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
	console.log(memoClosureTimes10(9));
} catch (error) {
	console.log('Task 4:' + error);
}

// eslint-disable-next-line no-unused-vars
var itemList = [230, 24, 26, 670, 1093, 1200, 12, 4, 34];

// function compare(list) {
// 	const result = {};
// 	for (let i = 0; i < list.length; i++) {
// 		var x;
// 	}
// }