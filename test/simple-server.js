// // Import the module
// const http = require('http');
// const port = 4000;
// const host = '127.0.0.1';

// // Create the server
// const server = http.createServer((req, res) => {
// 	res.statusCode = 200;
// 	res.setHeader('Content-Type', 'text/html');
// 	res.end('Hello World');
// });

// // Listen to server for a request
// server.listen(port, host, () => {
// 	console.log(`Server is running at http://${host}:${port}`);
// });


const uniqueSort = (arr) => {
	let breadcrumbs = [];

	let sorted = arr.sort((a, b) => a - b);
	// .filter((value, index, array) => {
	// 	return value !== array[index + 1];
	// });
	for (let i = 0; i < sorted.length; i++) {
		if (sorted[i] !== sorted[i + 1]) {
			breadcrumbs.push(sorted[i]);
		}
	}

	return breadcrumbs;

};

// console.log(uniqueSort([4, 2, 2, 3, 2, 2, 2]));
console.log(uniqueSort([1, 5, 2, 1]));