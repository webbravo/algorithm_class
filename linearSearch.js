function linearSearch(list, item) {
    let index = -1;
    for (let i = 0; i < list.length; i++)
        if (list[i] === item) index = list.indexOf(item);
    return index;
}

const search = linearSearch([2, 6, 7, 90, 103], 90);
console.log(search);